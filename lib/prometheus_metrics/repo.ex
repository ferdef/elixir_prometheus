defmodule PrometheusMetrics.Repo do
  use Ecto.Repo,
    otp_app: :prometheus_metrics,
    adapter: Ecto.Adapters.Postgres
end
