defmodule PrometheusMetricsWeb.PageController do
  use PrometheusMetricsWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
