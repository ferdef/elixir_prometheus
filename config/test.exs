use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :prometheus_metrics, PrometheusMetricsWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :prometheus_metrics, PrometheusMetrics.Repo,
  username: "root",
  password: "root",
  database: "prometheus_metrics_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
